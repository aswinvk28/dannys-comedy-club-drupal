<div class="<?php print !empty($classes) ? $classes : ''; ?>">
    <ul class="clearfix unstyled image_text">
        <li class="column_mini">
            <span class="black">Show: </span><span><a href="#" class="read-more black">Friday, 31 May 2013</a></span>
        </li>
        <li class="column_mini">
            <span class="black">Date: </span><span class="white">31-May-2013</span>
        </li>
        <li class="column_mini">
            <span class="black">Venue: </span><span><a href="#" class="read-more black">The United Services Club Haywards Heath</a></span>
        </li>
    </ul>
</div>