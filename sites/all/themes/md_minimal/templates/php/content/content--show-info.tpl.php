<article class="clearfix gray" itemprop="mainContentOfPage" itemscope itemtype="http://schema.org/WebPageElement">
    <header class="column_title">
        <h3>SHOW TIMES</h3>
    </header>
    <div class="row-fluid column_normal">
        <img class="span2" src="<?php echo base_path() . path_to_theme(); ?>/images/icons/shows/1360371788_Single Seater Djinn Chair.png" alt="Seat Icon" title="Shows opening hours" />
        <div class="span10 spaced_description">Danny’s Comedy Club has shows on the last Friday of every month. 
        Doors open at 7.30pm, with Curtain-up at 8.30pm.
        We recommend you arrive early to get the best seats. 
        </div>
    </div>
    <div class="column_normal row-fluid">
        <img src="<?php echo base_path() . path_to_theme(); ?>/images/icons/bar/1360397105_bar_beer.png" class="span2" alt="Drinks Icon" title="Reasonanbly priced bar" />
        <div class="span10 spaced_description">
            <p>The Club has a reasonably priced bar with discounts and special offers on Comedy Club nights, served by friendly, professional staff and accompanied by an excellent Danny's Comedy Club menu designed by the Birch's head chef.</p>
            <p>The audience are seated informally, cabaret-style, so you won't feel conspicuous. </p>
        </div>
    </div>
    <div class="column_normal row-fluid">
        <img src="<?php echo base_path() . path_to_theme(); ?>/images/icons/price/1360372090_currency_pound green.png" class="span2" alt="Pound Icon" title="Price for the Show" />
        <div class="span10 spaced_description">
            <p>The Box Office can be found at the entrance to the Club, just to the right of the entrance lobby. Admission is <span class="white fx-large">£7.00</span> per person, payable at the door on the night. </p>
            <p><span class="white fx-large">Advanced Tickets</span> are also available from the Hotel Reception at any time.</p>
            <p><i>We regret that we do not give refunds after <span class="white fx-large">9.00 pm.</span></i></p>
        </div>
    </div>
</article>