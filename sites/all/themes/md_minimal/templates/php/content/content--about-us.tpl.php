<div class="clearfix spaced_description white">
    <div class="row-fluid column_small">
        <div class="span6 content_block content_block_content mr20 border_radius_large">
            <p>Danny's Comedy Club offers comedy and other entertainment for the over 18's. Please be aware that much of the material will be adult in nature and those easily offended should stay at home with a mug of cocoa.</p>
        </div>
        <div>
            <p>Danny’s Comedy Club was founded in 2012 by Danny Kington.  Trained by the Redgrave family, he has a solid background in variety and musical theatre both on and offstage, working with many famous names over the last thirty or so years.</p>
            <p>Danny’s Comedy Club is fanatical about providing a platform for new and developing talent.  With the closure of so many theatres in recent decades the opportunity for new acts to develop and be spotted for the wider audience has all but disappeared.  If you think everything on telly these days is crap, that’s why and we agree with you.</p>
        </div>
    </div>
    <div class="row-fluid column_small">
        <div class="span6 content_block content_block_content mr20 border_radius_large">
            <p>Danny has also worked in film and TV with roles in Ken Loach’s The Wind that Shakes the Barley and the 2012 version of Great Expectations. Graduating with an Honours Degree in Modern Drama Studies from Brunel University London, Danny’s final thesis was on Stand-Up Comedy.</p>
        </div>
        <div>
            <p>There is however no compromise on quality and the acts perform to the highest professional standards.  The south of England is awash with talent and yet has almost nowhere for them to perform before a sizeable audience.</p>
        </div>
        <div class="spaced_description">
            <p>Danny’s Comedy Club exists to nurture and encourage artistes and in time expect to see some of them achieve fame and fortune.  Remember, you saw them here first.</p>
            <p class="column white flarge">Danny’s is different from other comedy clubs in offering wider forms of entertainment than purely stand-up; singers, ventriloquists, and many others help to form a more rounded night’s entertainment.</p>
        </div>
    </div>
</div>