<div class="clearfix">
    <section class="content_block_content">
        <div class="clearfix">
            <a name="show-progress"></a>

            <?php print theme_render_template(path_to_theme() . '/layouts/shows-progress.php', array()); ?>

            <a name="show-times"></a>
            <div class="column_title column_small">
                <h4>Show Times</h4>
            </div>
            <p class="column_small white">There will be a couple of comfort breaks during the performance and the bar will remain open after the show.</p>
            <p>Danny’s Comedy Club has shows on the last Friday of every month. Doors open at <span class="white">7.30pm</span>, with Curtain-up at <span class="white">8.30pm</span>.  We recommend you arrive early to get the best seats. </p>
            <a name="drinks-served"></a>
            <div class="column_title column_small">
                <h4>Drinks Served</h4>
            </div>
            <div>
                <p>The Club has a reasonably priced bar with discounts and special offers on Comedy Club nights, served by friendly, professional staff and accompanied by an excellent Danny's Comedy Club menu designed by the Birch's head chef.</p>
                <p>The audience are seated informally, cabaret-style, so you won't feel conspicuous. </p>
            </div>
            <a name="admission-fee"></a>
            <div class="column_title column_small">
                <h4>Admission</h4>
            </div>
            <div>
                <p>The Box Office can be found at the entrance to the Club, just to the right of the entrance lobby. Admission is <span class="white flarge">£7.00</span> per person, payable at the door on the night. </p>
                <p><span class="white flarge">Advanced Tickets</span> are also available from the Hotel Reception at any time.</p>
                <p><i>We regret that we do not give refunds after <span class="white flarge">9.00 pm.</span></i></p>
            </div>
        </div>
        <div class="clearfix content_block content_block_content column_small border_radius_large fsmall column">
            <p>Danny's Comedy Club offers comedy and other entertainment for the over 18's. Please be aware that much of the material will be adult in nature and those easily offended should stay at home with a mug of cocoa.</p>
        </div>
    </section>
    <section>
        <header class="column_title column_small">
            <h3>Large Groups, Stag & Hen</h3>
        </header>
        <div class="content_block content_block_content column_small border_radius_large">
            <p>Please let us know in advance if you are bringing a large group as we can’t move tables and chairs around to suit large groups on the night. We also recommend you order any food in advance directly with the Birch Hotel (see Venue page). 
            We encourage a good atmosphere and reasonable interaction with the audience but large single sex groups who turn up excessively drunk will be turned away at the door.
            We discourage stag and hen groups, as it’s a bit daft bringing your friends to a venue where they just want to chat, chat-up and get hammered. It spoils the comedy and annoys the other guests.
            </p>
        </div>
    </section>
</div>