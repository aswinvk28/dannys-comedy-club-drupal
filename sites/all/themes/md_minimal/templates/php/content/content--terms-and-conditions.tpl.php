<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<ol class="clearfix column_small terms-and-conditions">
    <li>
        <header><h3>INTRODUCTION</h3></header>
        <p>These terms of use govern your use of our website; by using our website, you agree to these terms of use in full.  If you disagree with these terms of use or any part of these terms of use, you must not use our website. </p>
        <p>Our website uses cookies.</p>
        <p>We will ask you to consent to our use of cookies in accordance with the terms of this policy when you first visit our website. / By using our website and agreeing to this policy, you consent to our use of cookies in accordance with the terms of this policy.</p>	
    </li>
    <li>
        <header><h3>COOKIES</h3></header>
        <ul class="unstyled">
            <li>
                <h4>(2.1)	Third party and analytics cookies</h4>
                <p>When you use our website, you may also be sent third party cookies.</p>
                <p>Our service providers may send you cookies. They may use the information they obtain from your use of their cookies:</p>
                <ul class="unstyled">
                        <li>(a)	&nbsp;to track your browser across multiple websites;</li>
                        <li>(b)	&nbsp;to build a profile of your web surfing; and</li>
                        <li>(c)	&nbsp;]to target advertisements which may be of particular interest to you.</li>
                </ul>
                <p>In addition, we use [Google Analytics] to analyse the use of this website. Google Analytics generates statistical and other information about 
                website use by means of cookies, which are stored on users' computers. The information generated relating to our website is used to create reports about 
                the use of the website. Google will store this information. Google's privacy policy is available at: <a href="http://www.google.com/privacypolicy.html">http://www.google.com/privacypolicy.html.</a></p>
            </li>
            <li>
                <h4>(2.2)	Cookies and personal information</h4>
                <p>Cookies do not contain any information that personally identifies you, but personal information that we store about you may be linked, by us, to the information stored in and obtained from cookies.</p>
            </li>
            <li>
                <h4>(2.3)	What information do we collect?</h4>
                <p>We may collect, store and use the following kinds of personal information:</p>
                <ul class="unstyled">
                        <li>(a)	&nbsp;information about your computer and about your visits to and use of this website (including your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views, website navigation and details);</li>
                        <li>(b) &nbsp;information that you provide to us for the purpose of registering with us (including details);</li>
                        <li>(e)	&nbsp;any other information that you choose to send to us; and</li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <header><h3>Intellectual property rights</h3></header>
        <p>Unless otherwise stated, we or our licensors own the intellectual property rights in the website and material on the website. Subject to the licence below, all these intellectual property rights are reserved.</p>
    </li>
    <li>
        <header><h3>Licence to use website</h3></header>
        <p>Unless otherwise stated, we or our licensors own the intellectual property rights in the website and material on the website. Subject to the licence below, all these intellectual property rights are reserved.</p>
        <p>You may view, download for caching purposes only, and print pages [or [other content]]  from the website for your own personal use, subject to the restrictions set out below and elsewhere in these terms of use.</p>
        <p>You must not:</p>
        <ul class="unstyled">
            <li>(a)	&nbsp;republish material from this website (including republication on another website);</li>
            <li>(b)	&nbsp;sell, rent or sub-license material from the website;</li>
            <li>(c)	&nbsp;show any material from the website in public;</li>
            <li>(d)	&nbsp;reproduce, duplicate, copy or otherwise exploit material on our website for a commercial purpose;</li>
            <li>(e)	&nbsp;edit or otherwise modify any material on the website; or</li>
            <li>(f)	&nbsp;redistribute material from this website except for content specifically and expressly made available for redistribution (such as our newsletter).
        </ul>
    </li>
    <li>
        <header><h3>Acceptable use</h3></header>
        <p>You must not use our website in any way that causes, or may cause, damage to the website or impairment of the availability or accessibility of the website; or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity.</p>
        <p>You must not use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software.</p>
        <p>You must not conduct any systematic or automated data collection activities (including, without limitation, scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent.</p>
        <p>You must not use our website to transmit or send unsolicited commercial communications.</p>
        <p>You must not use our website for any purposes related to marketing without our express written consent.</p>
    </li>
    <li>
        <header><h3>Restricted access</h3></header>
        <p>Access to certain areas of our website is restricted. We reserve the right to restrict access to other areas of our website, or indeed our whole website, at our discretion.</p>
    </li>
    <li>
        <header><h3>Limited warranties</h3></header>
        <p>We do not warrant the completeness or accuracy of the information published on this website; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.</p>
        <p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to this website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill).</p>
    </li>
    <li>
        <header><h3>Limitations and exclusions of liability </h3></header>
        <p>Nothing in these terms of use will: (a) limit or exclude our or your liability for death or personal injury resulting from negligence; (b) limit or exclude our or your liability for fraud or fraudulent misrepresentation; (c) limit any of our or your liabilities in any way that is not permitted under applicable law; or 
        (d) exclude any of our or your liabilities that may not be excluded under applicable law. </p>
        <p>The limitations and exclusions of liability set out in this Section and elsewhere in these terms of use: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under these terms of use or in relation to the subject matter of these terms of use, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.</p>
        <p>To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>
        <p>We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.</p>
        <p>We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.</p>
        <p>We will not be liable to you in respect of any loss or corruption of any data, database or software.</p>
        <p>We will not be liable to you in respect of any special, indirect or consequential loss or damage.</p>
    </li>
    <li>
        <header><h3>Indemnity</h3></header>
        <p>You hereby indemnify us and undertake to keep us indemnified against any losses, damages, costs, liabilities and expenses (including, without limitation, legal expenses and any amounts paid by us to a third party in settlement of a claim or dispute on the advice of our legal advisers) incurred or 
        suffered by us arising out of any breach by you of any provision of these terms of use, or arising out of any claim that you have breached any provision of these terms of use. </p>
    </li>
    <li>
        <header><h3>Breaches of these terms of use</h3></header>
        <p>Without prejudice to our other rights under these terms of use, if you breach these terms of use in any way, we may take such action as we deem appropriate to deal with the breach, 
        including suspending your access to the website, prohibiting you from accessing the website, blocking computers 
        using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.</p>
    </li>
    <li>
        <header><h3>Variation</h3></header>
        <p>We may revise these terms of use from time to time. Revised terms of use will apply to the use of our website from the date of publication of the revised terms of use on our website.</p>
    </li>
    <li>
        <header><h3>Assignment</h3></header>
        <p>We may transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms of use without notifying you or obtaining your consent.</p>
        <p>You may not transfer, sub-contract or otherwise deal with your rights and/or obligations under these terms of use. </p>
    </li>
    <li>
        <header><h3>Severability</h3></header>
        <p>If a provision of these terms of use is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect. If any unlawful 
        and/or unenforceable provision would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.</p> 
    </li>
    <li>
        <header><h3>Exclusion of third party rights</h3></header>
        <p>These terms of use are for the benefit of you and us, and are not intended to benefit any third party or be enforceable by any third party. The exercise of our and your rights in relation to these terms of use is not subject to the consent of any third party. </p>
    </li>
    <li>
        <header><h3>Entire agreement</h3></header>
        <p>Subject to the first paragraph of Section [8], these terms of use, together with our privacy policy,  
        constitute the entire agreement between you and us in relation to your use of our website and supersede all previous agreements in respect of your use of our website.
    </li>
    <li>
        <header><h3>Law and jurisdiction </h3></header>
        <p>These terms of use will be governed by and construed in accordance with English  law, and any disputes relating to these terms of use will be subject to the 
        non-exclusive  jurisdiction of the courts of England and Wales.</p>
    </li>
    <li>
        <header><h3>Linking Policy</h3></header>
        <ul class="unstyled">
            <li><h4>(18.1)	Status of linking policy</h4>
                <p>We welcome links to this website made in accordance with the terms of this linking policy.</p>
                <p>This linking policy is intended to assist you when linking to this website. / By using this website you agree to be bound by the provisions of this linking policy.</p>
            </li>
            <li>
                <h4>(18.2)	Links to this website</h4>
                <p>Links pointing to this website should not be misleading. </p>
                <p>Appropriate link text should always be used in links pointing to this website.</p>
                <p>From time to time we may update the URL structure of our website, and unless we agree in writing otherwise, all links should point to URL.</p>
                <p>You must not use our logo to link to this website (or otherwise) without our express written permission.</p>
                <p>You must not link to this website using any inline linking technique.</p>
                <p>You must not frame the content of this website or use any similar technology in relation to the content of this website.</p>
            </li>
            <li>
                <h4>(18.3)	Links from this website</h4>
                <p>This website includes links to other websites owned and operated by third parties. These links are not endorsements or recommendations. </p>
                <p>We have no control over the contents of third party websites, and we accept no responsibility for them or for any loss or damage that may arise from your use of them.</p>
            </li>
            <li>
                <h4>(18.4)	Removal of links</h4>
                <p>You agree that, should we request the deletion of a link to our website that is within your control, you will delete the link promptly.</p>
                <p>If you would like us to remove a link to your website that is included on this website, please contact us using the contact details below. Note that unless you have a legal right to demand removal, such removal will be at our discretion.</p>
            </li>
            <li>
                <h4>(18.5)	Changes to this linking policy</h4>
                <p>We may amend this linking policy at any time by publishing a new version on this website. </p>
            </li>
        </ul>
        <p>
    </li>
    <li>
        <header><h3>COPYRIGHT</h3></header>
        <ul class="unstyled">
                <li>
                    <h4>(19.1) Ownership of copyright</h4>
                    <p>We and our licensors  own the copyright in:
                    <ul class="unstyled">
                        <li>(a)	&nbsp;this website; and </li>
                        <li>(b)	&nbsp;the material on this website (including, without limitation, the text, computer code, artwork, photographs, images, music, audio material, video material and audio-visual material on this website).</li>
                    </ul>
                </li>
                <li>
                    <h4>(19.2)	Copyright licence</h4>
                    <p>We grant to you a worldwide, non-exclusive, royalty-free, revocable licence to:</p>
                    <ul class="unstyled">
                        <li>(a)	&nbsp;view this website and the material on this website on a computer or mobile device via a web browser;</li>
                        <li>(b)	&nbsp;copy and store this website and the material on this website in your web browser cache memory; and</li>
                        <li>(c)	&nbsp;print pages from this website for your own personal and non-commercial use.
                    </ul>
                    <p>We do not grant you any other rights in relation to this website or the material on this website. In other words, all other rights are reserved.</p>
                    <p>For the avoidance of doubt, you must not adapt, edit, change, transform, publish, republish, distribute, redistribute, broadcast, rebroadcast, or 
                    show or play in public this website or the material on this website (in any form or media) without our prior written permission. </p>
                </li>
                <li>
                    <h4>(19.3)	Data mining</h4>
                    <p>The automated and/or systematic collection of data from this website is prohibited.</p>
                </li>
                <li>
                    <h4>(19.4)	Permissions</h4>
                    <p>You may request permission to use the copyright materials on this website by writing to <a href="mailto:danny.kington@gmail.com">danny.kington@gmail.com</a>.</p>
                </li>
                <li>
                    <h4>(19.5)	Enforcement of copyright</h4>
                    <p>We take the protection of our copyright very seriously.</p>
                    <p>If we discover that you have used our copyright materials in contravention of the licence above, 
                    we may bring legal proceedings against you, seeking monetary damages and/or an injunction to stop you using those materials. You could also be ordered to pay legal costs.</p>
                    <p>If you become aware of any use of our copyright materials that contravenes or may contravene the licence above, 
                    please report this by email to <a href="mailto:danny.kington@gmail.com">danny.kington@gmail.com</a>.</p>
                </li>
                <li>
                    <h4>(19.6)	Infringing material</h4>
                    <p>If you become aware of any material on our website that you believe infringes your or any other person's copyright, please report this by email to <a href="mailto:danny.kington@gmail.com">danny.kington@gmail.com</a>.</p>
                </li>
        </ul>
    </li>
    <li>
        <header><h4>Credit</h4></header>
        <p>This document was created using an <a href="http://www.seqlegal.com/free-legal-documents/website-terms-and-conditions">SEQ Legal</a> template.</p>
    </li>
</ol>