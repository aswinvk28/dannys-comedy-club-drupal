<div class="column_title">
    <h3>Danny's Interview with Meridian FM</h3>
    <div class="clearfix column_small">
        <div id="club-audio"></div>
        <div id="jp_container_1" class="jp-audio">
            <div class="jp-type-single">
              <div class="jp-gui jp-interface">
                <ul class="jp-controls">
                  <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
                  <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
                  <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
                  <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
                  <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
                  <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
                </ul>
                <div class="jp-progress">
                  <div class="jp-seek-bar">
                    <div class="jp-play-bar"></div>
                  </div>
                </div>
                <div class="jp-volume-bar">
                  <div class="jp-volume-bar-value"></div>
                </div>
                <div class="jp-time-holder">
                  <div class="jp-current-time"></div>
                  <div class="jp-duration"></div>
                  <ul class="jp-toggles">
                    <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
                    <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
                  </ul>
                </div>
              </div>
              <div class="jp-title">
                <ul>
                  <li>Danny's Interview with Meridian FM</li>
                </ul>
              </div>
              <div class="jp-no-solution">
                <span>Update Required</span>
                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
              </div>
            </div>
        </div>
    </div>
    <div class="column_small clearfix">
        <p>
            <a class="span9" href="http://www.meridianfm.com/"><img src="/sites/default/files/resources/Meridian_FM_Logo.jpg" alt="Meridian FM Logo" title="" /></a>
            <span class="span3">Daniel Kington, the owner of the club, had an interview just before one of our shows.</span>
        </p>
    </div>
</div>